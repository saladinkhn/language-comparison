﻿using System;

namespace helloTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Enter two num!");
            int i = Convert.ToInt32(Console.ReadLine());
            int j = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"sum of {i} and {j} is equal to {i + j}");
        }
    }
}
